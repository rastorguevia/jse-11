package ru.rastorguev.tm.api.repository;

import ru.rastorguev.tm.entity.Session;

public interface ISessionRepository extends IRepository<Session> {

    boolean isContains(final Session session);
}

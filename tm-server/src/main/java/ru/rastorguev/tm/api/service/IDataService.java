package ru.rastorguev.tm.api.service;

public interface IDataService {

    void dataSaveBinary() throws Exception;

    void dataLoadBinary() throws Exception;

    void dataSaveJaxbXml() throws Exception;

    void dataLoadJaxbXml() throws Exception;

    void dataSaveJaxbJson() throws Exception;

    void dataLoadJaxbJson() throws Exception;

    void dataSaveFasterXmlXml() throws Exception;

    void dataLoadFasterXmlXml() throws Exception;

    void dataSaveFasterXmlJson() throws Exception;

    void dataLoadFasterXmlJson() throws Exception;
}

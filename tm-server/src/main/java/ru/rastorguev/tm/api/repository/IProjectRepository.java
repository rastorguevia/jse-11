package ru.rastorguev.tm.api.repository;

import ru.rastorguev.tm.entity.Project;

import java.util.List;

public interface IProjectRepository extends IRepository<Project> {

    void removeAllByUserId(final String userId);

    List<Project> findAllByUserId(final String userId);

    String getProjectIdByNumber(final int number);

    List<Project> findProjectsByInputAndUserId(final String input, final String userId);
}

package ru.rastorguev.tm.api.service;

import ru.rastorguev.tm.entity.AbstractEntity;

import java.util.Collection;
import java.util.List;

public interface IService<E extends AbstractEntity> {

    Collection<E> findAll();

    List<E> findAllList();

    E findOne(String entityId);

    E persist(E entity);

    E merge(E entity);

    E remove(String entityId);

    void removeAll();

    void loadFromDto(List<E> listOfEntities);
}

package ru.rastorguev.tm.api.service;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.entity.Session;
import ru.rastorguev.tm.enumerated.Role;
import ru.rastorguev.tm.error.AccessDeniedException;

public interface ISessionService extends IService<Session> {

    void validate (@Nullable final Session session) throws AccessDeniedException;

    void validateSessionAndRole (@Nullable final Session session, @NotNull final Role role) throws AccessDeniedException;
}

package ru.rastorguev.tm.api.repository;

import ru.rastorguev.tm.entity.User;

public interface IUserRepository extends IRepository<User> {

    void userRegistry(String login, String password) throws Exception;

    void adminRegistry(String login, String password) throws Exception;

    User findByLogin(final String login);

    boolean isExistByLogin(final String login);
}

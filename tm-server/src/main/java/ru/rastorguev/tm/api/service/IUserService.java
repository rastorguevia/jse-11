package ru.rastorguev.tm.api.service;

import ru.rastorguev.tm.entity.User;
import ru.rastorguev.tm.error.AccessDeniedException;

public interface IUserService extends IService<User> {

    User findByLogin(String login);

    boolean isExistByLogin(String login);

    User createUser(final String login, final String password) throws AccessDeniedException;

    User createAdmin(final String login, final String password) throws AccessDeniedException;

    void updateUserLogin(final String userId, final String login) throws AccessDeniedException;

    void updateUserPassword(final String userId, final String password, final String old) throws AccessDeniedException;
}

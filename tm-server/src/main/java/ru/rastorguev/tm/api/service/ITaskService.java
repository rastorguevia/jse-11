package ru.rastorguev.tm.api.service;

import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.entity.Task;

import java.util.List;

public interface ITaskService extends IService<Task> {

    void removeTaskListByProjectId(final String projectId);

    String getTaskIdByNumber(final int number, final List<Task> filteredListOfTasks);

    String getTaskIdByNumberAndProjectId(final int number, String projectId);

    List<Task> taskListByUserId(final String userId);

    List<Task> taskListByProjectIdSorted(@Nullable final String projectId, @Nullable final String sortType);

    List<Task> filteredTaskListByUserIdAndInput(final String userId, final String input);

    List<Task> taskListByProjectId(final String projectId);
}

package ru.rastorguev.tm.context;

import lombok.Getter;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.rastorguev.tm.api.repository.IProjectRepository;
import ru.rastorguev.tm.api.repository.ISessionRepository;
import ru.rastorguev.tm.api.repository.ITaskRepository;
import ru.rastorguev.tm.api.repository.IUserRepository;
import ru.rastorguev.tm.api.service.*;
import ru.rastorguev.tm.constant.Constant;
import ru.rastorguev.tm.endpoint.*;
import ru.rastorguev.tm.entity.User;
import ru.rastorguev.tm.enumerated.Role;
import ru.rastorguev.tm.repository.ProjectRepository;
import ru.rastorguev.tm.repository.SessionRepository;
import ru.rastorguev.tm.repository.TaskRepository;
import ru.rastorguev.tm.repository.UserRepository;
import ru.rastorguev.tm.service.*;
import javax.xml.ws.Endpoint;

@NoArgsConstructor
public final class Bootstrap implements ServiceLocator {

    @NotNull
    private final ITaskRepository taskRepository = new TaskRepository();

    @NotNull
    private final IProjectRepository projectRepository = new ProjectRepository(taskRepository);

    @NotNull
    private final IUserRepository userRepository = new UserRepository();

    @NotNull
    private final ISessionRepository sessionRepository = new SessionRepository();

    @Getter
    @NotNull
    private final ITaskService taskService = new TaskService(taskRepository);

    @Getter
    @NotNull
    private final IProjectService projectService = new ProjectService(projectRepository);

    @Getter
    @NotNull
    private final IUserService userService = new UserService(userRepository);

    @Getter
    @NotNull
    private final ISessionService sessionService = new SessionService(sessionRepository);

    @Getter
    @NotNull
    private final IDataService dataService = new DataService(this);

    @NotNull
    private final IDataEndpoint dataEndpoint = new DataEndpoint(this);

    @NotNull
    private final IProjectEndpoint projectEndpoint = new ProjectEndpoint(this);

    @NotNull
    private final ITaskEndpoint taskEndpoint = new TaskEndpoint(this);

    @NotNull
    private final IUserEndpoint userEndpoint = new UserEndpoint(this);

    @NotNull
    private final ISessionEndpoint sessionEndpoint = new SessionEndpoint(this);


    public void init() throws Exception {
        createUsers();
        startServer();
    }

    private void startServer() {
        Endpoint.publish(Constant.USER_ENDPOINT_URL, userEndpoint);
        System.out.println(Constant.USER_ENDPOINT_URL);

        Endpoint.publish(Constant.PROJECT_ENDPOINT_URL, projectEndpoint);
        System.out.println(Constant.PROJECT_ENDPOINT_URL);

        Endpoint.publish(Constant.TASK_ENDPOINT_URL, taskEndpoint);
        System.out.println(Constant.TASK_ENDPOINT_URL);

        Endpoint.publish(Constant.SESSION_ENDPOINT_URL, sessionEndpoint);
        System.out.println(Constant.SESSION_ENDPOINT_URL);

        Endpoint.publish(Constant.DATA_ENDPOINT_URL, dataEndpoint);
        System.out.println(Constant.DATA_ENDPOINT_URL);
    }

    private void createUsers() throws Exception {
        @NotNull final User user = new User();
        user.setId("b4134afc-23d7-4706-8133-3c0fdf63304b");
        user.setLogin("qwerty");
        user.setPassHash("qwerty");
        user.setRole(Role.USER);
        userService.persist(user);
        @NotNull final User admin = new User();
        admin.setId("aab8fb8f-8bbc-4480-b561-7140e8353d8a");
        admin.setLogin("admin");
        admin.setPassHash("qwerty");
        admin.setRole(Role.ADMINISTRATOR);
        userService.persist(admin);
    }
}
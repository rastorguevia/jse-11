package ru.rastorguev.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.api.service.ServiceLocator;
import ru.rastorguev.tm.entity.Session;
import ru.rastorguev.tm.entity.Task;
import ru.rastorguev.tm.enumerated.Role;
import ru.rastorguev.tm.error.AccessDeniedException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@NoArgsConstructor
@WebService(endpointInterface = "ru.rastorguev.tm.endpoint.ITaskEndpoint")
public class TaskEndpoint implements ITaskEndpoint {

    private ServiceLocator serviceLocator;

    public TaskEndpoint(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Override
    @WebMethod
    public Task createTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "task") @Nullable final Task task
    ) throws Exception {
        if (session == null) throw new AccessDeniedException("Access denied. Session does not exist.");
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().persist(task);
    }

    @Nullable
    @Override
    @WebMethod
    public Task updateTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "task") @Nullable final Task task
    ) throws Exception {
        if (session == null) throw new AccessDeniedException("Access denied. Session does not exist.");
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().merge(task);
    }

    @Nullable
    @Override
    @WebMethod
    public Task removeTask(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "taskId") @Nullable final String taskId
    ) throws Exception {
        if (session == null) throw new AccessDeniedException("Access denied. Session does not exist.");
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().remove(taskId);
    }

    @Override
    @WebMethod
    public void removeTaskListByProjectId(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "projectId") @Nullable final String projectId
    ) throws Exception {
        if (session == null) throw new AccessDeniedException("Access denied. Session does not exist.");
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getTaskService().removeTaskListByProjectId(projectId);
    }

    @Nullable
    @Override
    @WebMethod
    public String taskIdByNumberAndProjectId(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "number") final int number,
            @WebParam(name = "projectId") @Nullable final String projectId
    ) throws Exception {
        if (session == null) throw new AccessDeniedException("Access denied. Session does not exist.");
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().getTaskIdByNumberAndProjectId(number, projectId);
    }

    @NotNull
    @Override
    @WebMethod
    public List<Task> taskListByUserId(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "userId") @NotNull final String userId
    ) throws Exception {
        if (session == null) throw new AccessDeniedException("Access denied. Session does not exist.");
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().taskListByUserId(userId);
    }

    @NotNull
    @Override
    @WebMethod
    public List<Task> filteredTaskListByInput(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "input") @NotNull final String input
    ) throws Exception {
        if (session == null) throw new AccessDeniedException("Access denied. Session does not exist.");
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().filteredTaskListByUserIdAndInput(session.getUserId(), input);
    }

    @NotNull
    @Override
    @WebMethod
    public List<Task> getTaskListByProjectId(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "projectId") @NotNull final String projectId
    ) throws Exception {
        if (session == null) throw new AccessDeniedException("Access denied. Session does not exist.");
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().taskListByProjectId(projectId);
    }

    @NotNull
    @Override
    @WebMethod
    public List<Task> getTaskListByProjectIdSorted(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "projectId") @NotNull final String projectId,
            @WebParam(name = "sortType") @NotNull final String sortType
    ) throws Exception {
        if (session == null) throw new AccessDeniedException("Access denied. Session does not exist.");
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().taskListByProjectIdSorted(projectId, sortType);
    }

    @Override
    @WebMethod
    public void removeAllTasks(
            @WebParam(name = "session") @Nullable final Session session
    ) throws Exception {
        if (session == null) throw new AccessDeniedException("Access denied. Session does not exist.");
        serviceLocator.getSessionService().validateSessionAndRole(session, Role.ADMINISTRATOR);
        serviceLocator.getTaskService().removeAll();
    }

    @Override
    @WebMethod
    public Task findTask (
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "taskId") @Nullable final String taskId
    ) throws Exception {
        if (session == null) throw new AccessDeniedException("Access denied. Session does not exist.");
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getTaskService().findOne(taskId);
    }
}

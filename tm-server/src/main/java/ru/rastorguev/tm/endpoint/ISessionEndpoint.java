package ru.rastorguev.tm.endpoint;

import org.jetbrains.annotations.NotNull;
import ru.rastorguev.tm.entity.Session;
import ru.rastorguev.tm.error.AccessDeniedException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface ISessionEndpoint {

    @WebMethod
    Session createNewSession(
            @WebParam(name = "login") @NotNull final String login,
            @WebParam(name = "password") @NotNull final String password
    ) throws Exception;

    @WebMethod
    void removeSession(@WebParam(name = "session") @NotNull final Session session);
}

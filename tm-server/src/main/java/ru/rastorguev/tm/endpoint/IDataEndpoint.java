package ru.rastorguev.tm.endpoint;

import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.entity.Session;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;

@WebService
public interface IDataEndpoint {

    @WebMethod
    void dataSaveBinary(@WebParam(name = "session") @Nullable final Session session) throws Exception;

    @WebMethod
    void dataLoadBinary(@WebParam(name = "session") @Nullable final Session session) throws Exception;

    @WebMethod
    void dataSaveJaxbXml(@WebParam(name = "session") @Nullable final Session session) throws Exception;

    @WebMethod
    void dataLoadJaxbXml(@WebParam(name = "session") @Nullable final Session session) throws Exception;

    @WebMethod
    void dataSaveJaxbJson(@WebParam(name = "session") @Nullable final Session session) throws Exception;

    @WebMethod
    void dataLoadJaxbJson(@WebParam(name = "session") @Nullable final Session session) throws Exception;

    @WebMethod
    void dataSaveFasterXmlXml(@WebParam(name = "session") @Nullable final Session session) throws Exception;

    @WebMethod
    void dataLoadFasterXmlXml(@WebParam(name = "session") @Nullable final Session session) throws Exception;

    @WebMethod
    void dataSaveFasterXmlJson(@WebParam(name = "session") @Nullable final Session session) throws Exception;

    @WebMethod
    void dataLoadFasterXmlJson(@WebParam(name = "session") @Nullable final Session session) throws Exception;
}

package ru.rastorguev.tm.endpoint;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.api.service.ServiceLocator;
import ru.rastorguev.tm.entity.Project;
import ru.rastorguev.tm.entity.Session;
import ru.rastorguev.tm.enumerated.Role;
import ru.rastorguev.tm.error.AccessDeniedException;

import javax.jws.WebMethod;
import javax.jws.WebParam;
import javax.jws.WebService;
import java.util.List;

@NoArgsConstructor
@WebService(endpointInterface = "ru.rastorguev.tm.endpoint.IProjectEndpoint")
public class ProjectEndpoint implements IProjectEndpoint {

    private ServiceLocator serviceLocator;

    public ProjectEndpoint(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

    @Nullable
    @Override
    @WebMethod
    public Project createProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "project") @Nullable final Project project
            ) throws Exception {
        if (session == null) throw new AccessDeniedException("Access denied. Session does not exist.");
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().persist(project);
    }

    @Nullable
    @Override
    @WebMethod
    public Project updateProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "project") @Nullable final Project project
    ) throws Exception {
        if (session == null) throw new AccessDeniedException("Access denied. Session does not exist.");
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().merge(project);
    }

    @Nullable
    @Override
    @WebMethod
    public Project removeProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "projectId") @Nullable final String projectId
    ) throws Exception {
        if (session == null) throw new AccessDeniedException("Access denied. Session does not exist.");
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().remove(projectId);
    }

    @Override
    @WebMethod
    public void removeAllProjectsByUser(
            @WebParam(name = "session") @Nullable final Session session
    ) throws Exception {
        if (session == null) throw new AccessDeniedException("Access denied. Session does not exist.");
        serviceLocator.getSessionService().validate(session);
        serviceLocator.getProjectService().removeAllByUserId(session.getUserId());
    }

    @NotNull
    @Override
    @WebMethod
    public List<Project> findAllProjectsForUser(
            @WebParam(name = "session") @Nullable final Session session
    ) throws Exception {
        if (session == null) throw new AccessDeniedException("Access denied. Session does not exist.");
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findAllByUserId(session.getUserId());
    }


    @NotNull
    @Override
    @WebMethod
    public List<Project> findAllProjectsForUserSorted(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "sortType") @Nullable final String sortType
    ) throws Exception {
        if (session == null) throw new AccessDeniedException("Access denied. Session does not exist.");
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findAllByUserIdSorted(session.getUserId(), sortType);
    }

    @NotNull
    @Override
    @WebMethod
    public List<Project> findProjectsByUserIdAndInput(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "input") @Nullable final String input
    ) throws Exception {
        if (session == null) throw new AccessDeniedException("Access denied. Session does not exist.");
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findProjectsByInputAndUserId(input, session.getUserId());
    }

    @Nullable
    @Override
    @WebMethod
    public String projectIdByNumber(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "number") final int number
    ) throws Exception {
        if (session == null) throw new AccessDeniedException("Access denied. Session does not exist.");
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().getProjectIdByNumberForUser(number, session.getUserId());
    }

    @Override
    @WebMethod
    public void removeAllProjects(
            @WebParam(name = "session") @Nullable final Session session
    ) throws Exception {
        if (session == null) throw new AccessDeniedException("Access denied. Session does not exist.");
        serviceLocator.getSessionService().validateSessionAndRole(session, Role.ADMINISTRATOR);
        serviceLocator.getProjectService().removeAll();
    }

    @Nullable
    @Override
    @WebMethod
    public Project findProject(
            @WebParam(name = "session") @Nullable final Session session,
            @WebParam(name = "projectId") @Nullable final String projectId
    ) throws Exception {
        if (session == null) throw new AccessDeniedException("Access denied. Session does not exist.");
        serviceLocator.getSessionService().validate(session);
        return serviceLocator.getProjectService().findOne(projectId);
    }
}

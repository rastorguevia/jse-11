package ru.rastorguev.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;

import java.util.UUID;

@NoArgsConstructor
public abstract class AbstractEntity {

    @Getter
    @Setter
    @NotNull
    private String id = UUID.randomUUID().toString();
}
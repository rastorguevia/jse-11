package ru.rastorguev.tm.entity;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import ru.rastorguev.tm.enumerated.Role;
import ru.rastorguev.tm.error.AccessDeniedException;

import java.io.Serializable;

import static ru.rastorguev.tm.util.MD5Util.mdHashCode;

@Getter
@Setter
@NoArgsConstructor
public final class User extends AbstractEntity implements Serializable {

    @NotNull
    private String login = "";

    @NotNull
    private String passHash = "";

    @NotNull
    private Role role = Role.USER;

    public User(final @NotNull Role role) {
        this.role = role;
    }

    public User(final @NotNull String login, final @NotNull String passHash, final @NotNull Role role) throws AccessDeniedException {
        this.login = login;
        this.passHash = mdHashCode(passHash);
        this.role = role;
    }

    public void setPassHash(final @NotNull String passHash) throws AccessDeniedException {
        this.passHash = mdHashCode(passHash);
    }
}

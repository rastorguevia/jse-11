package ru.rastorguev.tm.enumerated;

import lombok.Getter;
import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;

@RequiredArgsConstructor
public enum Status {
    PLANNED ("Planned"),
    IN_PROGRESS ("InProgress"),
    DONE ("Done");

    @Getter
    @NotNull
    private final String displayName;
}

package ru.rastorguev.tm.comparator;

import org.jetbrains.annotations.NotNull;
import ru.rastorguev.tm.api.entity.ComparableEntity;

import java.util.Comparator;

public final class EntityComparator {

    @NotNull
    public static Comparator<ComparableEntity> comparatorCreationDate =
            Comparator.comparingLong(ComparableEntity::getCreationDate);

    @NotNull
    public static Comparator<ComparableEntity> comparatorStartDate = new Comparator<ComparableEntity>() {
        @Override
        public int compare(ComparableEntity o1, ComparableEntity o2) {
            return o1.getStartDate().compareTo(o2.getStartDate());
        }
    };

    @NotNull
    public static Comparator<ComparableEntity> comparatorEndDate = new Comparator<ComparableEntity>() {
        @Override
        public int compare(ComparableEntity o1, ComparableEntity o2) {
            return o1.getEndDate().compareTo(o2.getEndDate());
        }
    };

    @NotNull
    public static Comparator<ComparableEntity> comparatorStatus = new Comparator<ComparableEntity>() {
        @Override
        public int compare(ComparableEntity o1, ComparableEntity o2) {
            return o1.getStatus().compareTo(o2.getStatus());
        }
    };
}

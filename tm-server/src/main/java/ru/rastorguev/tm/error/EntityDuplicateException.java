package ru.rastorguev.tm.error;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public final class EntityDuplicateException extends RuntimeException{

    public EntityDuplicateException(final String message) {
        super(message);
    }
}
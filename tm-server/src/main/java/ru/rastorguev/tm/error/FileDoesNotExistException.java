package ru.rastorguev.tm.error;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public final class FileDoesNotExistException extends Exception{

    public FileDoesNotExistException(final String message) {
        super(message);
    }
}

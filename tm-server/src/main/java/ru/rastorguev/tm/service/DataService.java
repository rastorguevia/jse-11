package ru.rastorguev.tm.service;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.dataformat.xml.XmlMapper;
import org.eclipse.persistence.jaxb.JAXBContextFactory;
import org.eclipse.persistence.jaxb.JAXBUnmarshaller;
import org.eclipse.persistence.jaxb.MarshallerProperties;
import org.eclipse.persistence.jaxb.UnmarshallerProperties;
import org.eclipse.persistence.oxm.MediaType;
import org.jetbrains.annotations.NotNull;
import ru.rastorguev.tm.api.service.IDataService;
import ru.rastorguev.tm.api.service.ServiceLocator;
import ru.rastorguev.tm.dto.DataTransferObject;
import ru.rastorguev.tm.error.FileDoesNotExistException;

import javax.xml.bind.JAXBContext;
import javax.xml.bind.Marshaller;
import javax.xml.bind.Unmarshaller;
import javax.xml.transform.stream.StreamSource;
import java.io.*;
import java.nio.file.Files;
import java.text.SimpleDateFormat;

public final class DataService implements IDataService {

    private final ServiceLocator serviceLocator;

    public DataService(@NotNull final ServiceLocator serviceLocator) {
        this.serviceLocator = serviceLocator;
    }

   @Override
    public void dataSaveBinary() throws Exception {
       @NotNull final File file = new File("/data.bin");
       Files.deleteIfExists(file.toPath());
       Files.createFile(file.toPath());

       @NotNull final ObjectOutputStream out = new ObjectOutputStream(new FileOutputStream(file));
       @NotNull final DataTransferObject dto = new DataTransferObject();
       dto.loadDto(serviceLocator);
       out.writeObject(dto);
       out.flush();
       out.close();
    }

    @Override
    public void dataLoadBinary() throws Exception {
        @NotNull final File file = new File("/data.bin");
        if (!Files.exists(file.toPath())) throw new FileDoesNotExistException("File does not exist");

        @NotNull final ObjectInputStream in = new ObjectInputStream(new FileInputStream(file));
        @NotNull final DataTransferObject dto = (DataTransferObject) in.readObject();
        dto.loadFromDto(serviceLocator);
        in.close();
    }

    @Override
    public void dataSaveJaxbXml() throws Exception {
        @NotNull final File file = new File("./data-jaxb.xml");
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());

        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(DataTransferObject.class);
        @NotNull final Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true);

        @NotNull final DataTransferObject dto = new DataTransferObject();
        dto.loadDto(serviceLocator);
        marshaller.marshal(dto, new FileOutputStream(file));
    }

    @Override
    public void dataLoadJaxbXml() throws Exception {
        @NotNull final File file = new File("./data-jaxb.xml");
        if (!Files.exists(file.toPath())) throw new FileDoesNotExistException("File does not exist");

        @NotNull final JAXBContext jaxbContext = JAXBContext.newInstance(DataTransferObject.class);
        @NotNull final Unmarshaller unmarshaller = jaxbContext.createUnmarshaller();

        @NotNull final DataTransferObject dto = (DataTransferObject) unmarshaller.unmarshal(new FileInputStream(file));
        dto.loadFromDto(serviceLocator);
    }

    @Override
    public void dataSaveJaxbJson() throws Exception {
        @NotNull final File file = new File("./data-jaxb.json");
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());

        @NotNull final JAXBContext jaxbContext = JAXBContextFactory.createContext(new Class[]{DataTransferObject.class}, null);
        @NotNull final Marshaller marshaller = jaxbContext.createMarshaller();
        marshaller.setProperty(Marshaller.JAXB_FORMATTED_OUTPUT, true); //pretty
        marshaller.setProperty(MarshallerProperties.MEDIA_TYPE, MediaType.APPLICATION_JSON);
        marshaller.setProperty(MarshallerProperties.JSON_INCLUDE_ROOT, false);
        marshaller.setProperty(MarshallerProperties.JSON_WRAPPER_AS_ARRAY_NAME, true);

        @NotNull final DataTransferObject dto = new DataTransferObject();
        dto.loadDto(serviceLocator);
        marshaller.marshal(dto, new FileOutputStream(file));
    }

    @Override
    public void dataLoadJaxbJson() throws Exception {
        @NotNull final File file = new File("./data-jaxb.json");
        if (!Files.exists(file.toPath())) throw new FileDoesNotExistException("File does not exist");

        @NotNull final org.eclipse.persistence.jaxb.JAXBContext jaxbContext =
                (org.eclipse.persistence.jaxb.JAXBContext) JAXBContextFactory.createContext(new Class[]{DataTransferObject.class}, null);
        @NotNull final JAXBUnmarshaller unmarshaller = jaxbContext.createUnmarshaller();
        unmarshaller.setProperty(UnmarshallerProperties.MEDIA_TYPE, MediaType.APPLICATION_JSON);
        unmarshaller.setProperty(UnmarshallerProperties.JSON_INCLUDE_ROOT, false);
        unmarshaller.setProperty(MarshallerProperties.JSON_WRAPPER_AS_ARRAY_NAME, true);

        @NotNull final DataTransferObject dto =
                (DataTransferObject) unmarshaller.unmarshal(new StreamSource(file), DataTransferObject.class).getValue();
        dto.loadFromDto(serviceLocator);
    }

    @Override
    public void dataSaveFasterXmlXml() throws Exception {
        @NotNull final File file = new File("./data-fasterxml.xml");
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());

        @NotNull final XmlMapper mapper = new XmlMapper();
        mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX"));

        @NotNull final DataTransferObject dto = new DataTransferObject();
        dto.loadDto(serviceLocator);

        mapper.writerWithDefaultPrettyPrinter().writeValue(file, dto);
    }

    @Override
    public void dataLoadFasterXmlXml() throws Exception {
        @NotNull final File file = new File("./data-fasterxml.xml");
        if (!Files.exists(file.toPath())) throw new FileDoesNotExistException("File does not exist");

        @NotNull final XmlMapper mapper = new XmlMapper();
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

        @NotNull final DataTransferObject dto = mapper.readValue(file, DataTransferObject.class);
        dto.loadFromDto(serviceLocator);
    }

    @Override
    public void dataSaveFasterXmlJson() throws Exception {
        @NotNull final File file = new File("./data-fasterxml.json");
        Files.deleteIfExists(file.toPath());
        Files.createFile(file.toPath());

        @NotNull final ObjectMapper mapper = new ObjectMapper();
        mapper.setDateFormat(new SimpleDateFormat("yyyy-MM-dd'T'HH:mm:ss.SSSXXX"));

        @NotNull final DataTransferObject dto = new DataTransferObject();
        dto.loadDto(serviceLocator);

        mapper.writerWithDefaultPrettyPrinter().writeValue(file, dto);
    }

    @Override
    public void dataLoadFasterXmlJson() throws Exception {
        @NotNull final File file = new File("./data-fasterxml.json");
        if (!Files.exists(file.toPath())) throw new FileDoesNotExistException("File does not exist");

        @NotNull final ObjectMapper mapper = new ObjectMapper();
        mapper.disable(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES);

        @NotNull final DataTransferObject dto = mapper.readValue(file, DataTransferObject.class);
        dto.loadFromDto(serviceLocator);
    }

}

package ru.rastorguev.tm.service;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.api.repository.IRepository;
import ru.rastorguev.tm.api.repository.ITaskRepository;
import ru.rastorguev.tm.api.service.ITaskService;
import ru.rastorguev.tm.entity.Project;
import ru.rastorguev.tm.entity.Task;

import java.util.Collection;
import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

import static ru.rastorguev.tm.comparator.EntityComparator.*;
import static ru.rastorguev.tm.comparator.EntityComparator.comparatorStatus;

@RequiredArgsConstructor
public final class TaskService extends AbstractService<Task> implements ITaskService {

    @NotNull
    private final ITaskRepository taskRepository;

    @NotNull
    @Override
    public IRepository<Task> getRepository() {
        return taskRepository;
    }

    @Override
    public void removeTaskListByProjectId(@Nullable final String projectId) {
        if (projectId == null || projectId.isEmpty()) return;
        taskRepository.removeTaskListByProjectId(projectId);
    }

    @Nullable
    @Override
    public String getTaskIdByNumber(final int number, @Nullable final List<Task> filteredListOfTasks) {
        if (filteredListOfTasks == null) return null;
        for (@NotNull final Task task: filteredListOfTasks) {
            if (task.getId().equals(filteredListOfTasks.get(number - 1).getId())) {
                return task.getId();
            }
        }
        return null;
    }

    @Nullable
    @Override
    public String getTaskIdByNumberAndProjectId(final int number, @Nullable String projectId) {
        if (projectId == null || projectId.isEmpty()) return null;
        @NotNull List<Task> filteredListOfTasks = taskListByProjectId(projectId);
        if (filteredListOfTasks == null) return null;
        for (@NotNull final Task task: filteredListOfTasks) {
            if (task.getId().equals(filteredListOfTasks.get(number - 1).getId())) {
                return task.getId();
            }
        }
        return null;
    }

    @NotNull
    @Override
    public List<Task> taskListByUserId(@NotNull final String userId) {
        if (userId.isEmpty()) return Collections.emptyList();;
        return taskRepository.taskListByUserId(userId);
    }

    @NotNull
    @Override
    public List<Task> filteredTaskListByUserIdAndInput(@NotNull final String userId, @NotNull final String input) {
        if (userId == null || userId.isEmpty()) return Collections.emptyList();
        if (input == null || input.isEmpty()) return Collections.emptyList();
        return taskRepository.filteredTaskListByUserIdAndInput(userId, input);
    }

    @NotNull
    @Override
    public List<Task> taskListByProjectId(@Nullable final String projectId) {
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        return taskRepository.taskListByProjectId(projectId);
    }

    @NotNull
    @Override
    public List<Task> taskListByProjectIdSorted(@Nullable final String projectId, @Nullable final String sortType) {
        if (projectId == null || projectId.isEmpty()) return Collections.emptyList();
        if (sortType == null || sortType.isEmpty()) return Collections.emptyList();

        List<Task> taskList = new LinkedList<>(taskRepository.taskListByProjectId(projectId));
        if ("bycreationdate".equals(sortType.toLowerCase())) taskList.sort(comparatorCreationDate);
        if ("byenddate".equals(sortType.toLowerCase())) taskList.sort(comparatorEndDate);
        if ("bystartdate".equals(sortType.toLowerCase())) taskList.sort(comparatorStartDate);
        if ("bystatus".equals(sortType.toLowerCase())) taskList.sort(comparatorStatus);

        return taskList;
    }
}
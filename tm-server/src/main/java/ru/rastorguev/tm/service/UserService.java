package ru.rastorguev.tm.service;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.api.repository.IRepository;
import ru.rastorguev.tm.api.repository.IUserRepository;
import ru.rastorguev.tm.api.service.IUserService;
import ru.rastorguev.tm.entity.User;
import ru.rastorguev.tm.enumerated.Role;
import ru.rastorguev.tm.error.AccessDeniedException;
import ru.rastorguev.tm.util.MD5Util;

@RequiredArgsConstructor
public final class UserService extends AbstractService<User> implements IUserService {

    @NotNull
    private final IUserRepository userRepository;

    @NotNull
    @Override
    public IRepository<User> getRepository() {
        return userRepository;
    }

    @Nullable
    @Override
    public User findByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) return null;
        return userRepository.findByLogin(login);
    }

    @Override
    public boolean isExistByLogin(@Nullable final String login) {
        if (login == null || login.isEmpty()) return false;
        return userRepository.isExistByLogin(login);
    }

    @Nullable
    @Override
    public User createUser(@Nullable final String login, @Nullable final String password) throws AccessDeniedException {
        if (login == null || login.isEmpty()) throw new AccessDeniedException("Wrong login. Try again.");
        if (password == null || password.isEmpty()) throw new AccessDeniedException("Wrong password. Try again.");
        boolean isExist = isExistByLogin(login);
        if (isExist) throw new AccessDeniedException("User already exist.");
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPassHash(password);
        return persist(user);
    }

    @Nullable
    @Override
    public User createAdmin(@Nullable final String login, @Nullable final String password) throws AccessDeniedException {
        if (login == null || login.isEmpty()) throw new AccessDeniedException("Wrong login. Try again.");
        if (password == null || password.isEmpty()) throw new AccessDeniedException("Wrong password. Try again.");
        boolean isExist = isExistByLogin(login);
        if (isExist) throw new AccessDeniedException("User already exist.");
        @NotNull final User user = new User();
        user.setLogin(login);
        user.setPassHash(password);
        user.setRole(Role.ADMINISTRATOR);
        return persist(user);
    }

    @Override
    public void updateUserLogin(@Nullable final String userId, @Nullable final String login) throws AccessDeniedException {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException("User does not exist");
        if (login == null || login.isEmpty()) throw new AccessDeniedException("Login is empty");
        @Nullable final User user = findOne(userId);
        if (user == null) throw new AccessDeniedException("User does not exist");
        user.setLogin(login);
        merge(user);
    }

    @Override
    public void updateUserPassword(
            @Nullable final String userId,
            @Nullable final String password,
            @Nullable final String old
    ) throws AccessDeniedException
    {
        if (userId == null || userId.isEmpty()) throw new AccessDeniedException("User does not exist");
        if (password == null || password.isEmpty()) throw new AccessDeniedException("Wrong password. Try again.");
        if (old == null || old.isEmpty()) throw new AccessDeniedException("Wrong old password. Try again.");
        @Nullable final User user = findOne(userId);
        if (user == null) throw new AccessDeniedException("User does not exist");
        final boolean isOldPassEquals = user.getPassHash().equals(MD5Util.mdHashCode(old));
        if (!isOldPassEquals) throw new AccessDeniedException("Old password did not match.");
        user.setPassHash(password);
        merge(user);

    }
}
package ru.rastorguev.tm.service;

import lombok.RequiredArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.api.repository.IRepository;
import ru.rastorguev.tm.api.repository.ISessionRepository;
import ru.rastorguev.tm.api.service.ISessionService;
import ru.rastorguev.tm.entity.Session;
import ru.rastorguev.tm.enumerated.Role;
import ru.rastorguev.tm.error.AccessDeniedException;

@RequiredArgsConstructor
public class SessionService extends AbstractService<Session> implements ISessionService {

    @NotNull
    private final ISessionRepository sessionRepository;

    @Override
    public @NotNull IRepository<Session> getRepository() {
        return sessionRepository;
    }

    @Override
    public void validate (@Nullable final Session session) throws AccessDeniedException {

        if (session == null) throw new AccessDeniedException("Access denied.");

        @Nullable final String userId = session.getUserId();
        @Nullable final String signature = session.getSignature();
        @Nullable final Role role = session.getRole();
        if (userId == null || userId.isEmpty() || signature == null || signature.isEmpty() || role == null) {
            throw new AccessDeniedException("Access denied.");
        }

        if (!sessionRepository.isContains(session)) throw new AccessDeniedException("Access denied.");

        final long timeOfExistence = System.currentTimeMillis() - session.getCreateSessionDate().getTime();
        //12 часов
        if (timeOfExistence > 43200000) {
            sessionRepository.remove(session.getId());
            throw new AccessDeniedException("Session is over.");
        }
    }

    public void validateSessionAndRole (@Nullable final Session session, @NotNull final Role role) throws AccessDeniedException {
        if (session == null) throw new AccessDeniedException("Access denied.");
        validate(session);
        if (!role.equals(session.getRole())) throw new AccessDeniedException("Access denied. Role type is not allowed.");
    }


}

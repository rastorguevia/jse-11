package ru.rastorguev.tm.constant;

public class Constant {

    public static final String SALT = "saltwhitepoison";

    public static final int CYCLE = 10;

    public static final String DATA_ENDPOINT_URL = "http://localhost:8080/DataEndpoint?wsdl";

    public static final String SESSION_ENDPOINT_URL = "http://localhost:8080/SessionEndpoint?wsdl";

    public static final String USER_ENDPOINT_URL = "http://localhost:8080/UserEndpoint?wsdl";

    public static final String PROJECT_ENDPOINT_URL = "http://localhost:8080/ProjectEndpoint?wsdl";

    public static final String TASK_ENDPOINT_URL = "http://localhost:8080/TaskEndpoint?wsdl";
}

package ru.rastorguev.tm.util;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.constant.Constant;
import ru.rastorguev.tm.error.AccessDeniedException;

public class SignatureUtil {

    @Nullable
    public static String sign(@Nullable final Object value) throws AccessDeniedException {
        try {
            @NotNull final ObjectMapper objectMapper = new ObjectMapper();
            @NotNull final String json = objectMapper.writeValueAsString(value);
            return signJson(json);
        } catch (final JsonProcessingException e) {
            return null;
        }
    }

    @Nullable
    public static String signJson(@Nullable final String json) throws AccessDeniedException {
        if (json == null || json.isEmpty()) return null;
        @Nullable String result = json;
        for (int i = 0; i < Constant.CYCLE; i++) {
            result = MD5Util.mdHashCode(Constant.SALT + result + Constant.SALT);
        }
        return result;
    }
}
package ru.rastorguev.tm.repository;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.api.repository.ISessionRepository;
import ru.rastorguev.tm.entity.Session;

import java.util.Objects;

@NoArgsConstructor
public final class SessionRepository extends AbstractRepository<Session> implements ISessionRepository {

    public boolean isContains(@NotNull final Session session) {
        @Nullable final Session checkThisSession = map.get(session.getId());
        if (checkThisSession == null) return false;

        final boolean isUserIdEquals = Objects.equals(checkThisSession.getUserId(), session.getUserId());
        final boolean isCreateDateEquals = Objects.equals(checkThisSession.getCreateSessionDate(), session.getCreateSessionDate());
        final boolean isSignatureEquals = Objects.equals(checkThisSession.getSignature(), session.getSignature());
        final boolean isRoleEquals = Objects.equals(checkThisSession.getRole(), session.getRole());

        return isUserIdEquals && isCreateDateEquals && isSignatureEquals && isRoleEquals;
    }
}

package ru.rastorguev.tm.repository;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.api.repository.IUserRepository;
import ru.rastorguev.tm.entity.User;
import ru.rastorguev.tm.enumerated.Role;

import java.util.LinkedList;
import java.util.List;

@NoArgsConstructor
public final class UserRepository extends AbstractRepository<User> implements IUserRepository {

    @Override
    public void userRegistry(@NotNull final String login, @NotNull final String password) throws Exception{
        @NotNull final User user = new User(login, password, Role.USER);
        persist(user);
    }

    @Override
    public void adminRegistry(@NotNull final String login, @NotNull final String password) throws Exception{
        @NotNull final User admin = new User(login, password, Role.ADMINISTRATOR);
        persist(admin);
    }

    @Nullable
    @Override
    public User findByLogin(@NotNull final String login) {
        @NotNull final List<User> listOfUsers = new LinkedList<>();
        listOfUsers.addAll(findAll());
        for (@NotNull final User user : listOfUsers) {
            if (user.getLogin().equals(login)) return user;
        }
        return null;
    }

    @Override
    public boolean isExistByLogin(@NotNull final String login) {
        @NotNull final List<User> listOfUsers = new LinkedList<>();
        listOfUsers.addAll(findAll());
        for (@NotNull final User user : listOfUsers) {
            if (user.getLogin().equals(login)) return true;
        }
        return false;
    }
}
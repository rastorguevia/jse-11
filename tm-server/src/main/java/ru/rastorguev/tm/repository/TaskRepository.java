package ru.rastorguev.tm.repository;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import ru.rastorguev.tm.api.repository.ITaskRepository;
import ru.rastorguev.tm.entity.Task;

import java.util.*;

@NoArgsConstructor
public final class TaskRepository extends AbstractRepository<Task> implements ITaskRepository {

    @Override
    public void removeTaskListByProjectId(@NotNull final String projectId) {
        @NotNull final List<Task> listOfTasks = new LinkedList<>();
        listOfTasks.addAll(findAll());
        for (@NotNull final Task task: listOfTasks) {
            if (task.getProjectId().equals(projectId)) {
                remove(task.getId());
            }
        }
    }

    @NotNull
    @Override
    public List<Task> taskListByUserId (@NotNull final String userId) {
        @NotNull final List<Task> listOfTasks = new LinkedList<>();
        @NotNull final List<Task> listOfTasksByUserId = new LinkedList<>();
        listOfTasks.addAll(findAll());
        for (@NotNull final Task task : listOfTasks) {
            if (task.getUserId().equals(userId)) {
                listOfTasksByUserId.add(task);
            }
        }
        return listOfTasksByUserId;
    }

    @NotNull
    @Override
    public List<Task> filteredTaskListByUserIdAndInput (@NotNull final String userId, @NotNull final String input) {
        @NotNull List<Task> listOfTasksByUserId = taskListByUserId(userId);
        @NotNull List<Task> filteredListOfTasksByUserId = new LinkedList<>();
        for (@NotNull final Task task : listOfTasksByUserId) {
            @NotNull final boolean nameContains = task.getName().contains(input);
            @NotNull final boolean descriptionContains = task.getDescription().contains(input);

            if (nameContains || descriptionContains) {
                filteredListOfTasksByUserId.add(task);
            }
        }
        return filteredListOfTasksByUserId;
    }

    @NotNull
    @Override
    public List<Task> taskListByProjectId (@NotNull final String projectId) {
        @NotNull final List<Task> listOfTasks = new LinkedList<>();
        @NotNull final List<Task> listOfTasksByProjectId = new LinkedList<>();
        listOfTasks.addAll(findAll());
        for (@NotNull final Task task : listOfTasks) {
            if (task.getProjectId().equals(projectId)) {
                listOfTasksByProjectId.add(task);
            }
        }
        return listOfTasksByProjectId;
    }
}
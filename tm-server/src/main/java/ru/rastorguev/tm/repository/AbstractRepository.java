package ru.rastorguev.tm.repository;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.api.repository.IRepository;
import ru.rastorguev.tm.entity.AbstractEntity;
import ru.rastorguev.tm.error.EntityDuplicateException;

import java.util.*;

@NoArgsConstructor
public abstract class AbstractRepository<E extends AbstractEntity> implements IRepository<E> {

    @NotNull
    protected final Map<String, E> map = new LinkedHashMap<>();

    @NotNull
    @Override
    public Collection<E> findAll() {
        return map.values();
    }

    @NotNull
    @Override
    public List<E> findAllList() {
        return new ArrayList<>(map.values());
    }

    @Nullable
    @Override
    public E findOne(final @NotNull String entityId) {
        return map.get(entityId);
    }

    @NotNull
    @Override
    public E persist(final @NotNull E entity) {
        if (map.containsKey(entity.getId())) throw new EntityDuplicateException("Entity already exist");
        map.put(entity.getId(), entity);
        return entity;
    }

    @NotNull
    @Override
    public E merge(final @NotNull E entity) {
        map.put(entity.getId(), entity);
        return entity;
    }

    @NotNull
    @Override
    public E remove(@NotNull final String entityId) {
        return map.remove(entityId);
    }

    @Override
    public void removeAll() {
        map.clear();
    }

    @Override
    public void loadFromDto(@NotNull final List<E> listOfEntities) {
        for (@NotNull final E entity : listOfEntities) {
            if (map.containsKey(entity.getId())) continue;
            map.put(entity.getId(), entity);
        }
    }
}
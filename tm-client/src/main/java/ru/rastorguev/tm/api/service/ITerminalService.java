package ru.rastorguev.tm.api.service;

public interface ITerminalService {

    String nextLine();

    void close();
}

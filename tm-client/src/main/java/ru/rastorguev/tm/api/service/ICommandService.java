package ru.rastorguev.tm.api.service;

import ru.rastorguev.tm.command.AbstractCommand;

import java.util.Map;

public interface ICommandService {

    Map<String, AbstractCommand> getCommandMap();
}

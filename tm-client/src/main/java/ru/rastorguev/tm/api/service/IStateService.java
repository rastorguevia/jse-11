package ru.rastorguev.tm.api.service;


import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.endpoint.Role;
import ru.rastorguev.tm.endpoint.Session;

public interface IStateService {

    Session getSession();

    void setSession(Session session);

    boolean isAuth();

    boolean isRolesAllowed(@Nullable final Role... roles);
}

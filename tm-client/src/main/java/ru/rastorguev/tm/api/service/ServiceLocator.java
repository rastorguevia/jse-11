package ru.rastorguev.tm.api.service;

import ru.rastorguev.tm.endpoint.*;

public interface ServiceLocator {

    ITerminalService getTerminalService();

    ICommandService getCommandService();

    IStateService getStateService();

    RuRastorguevTmEndpointIDataEndpoint getDataEndpoint();

    IProjectEndpoint getProjectEndpoint();

    ITaskEndpoint getTaskEndpoint();

    IUserEndpoint getUserEndpoint();

    ISessionEndpoint getSessionEndpoint();
}

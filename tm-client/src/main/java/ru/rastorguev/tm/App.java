package ru.rastorguev.tm;

import org.jetbrains.annotations.NotNull;
import ru.rastorguev.tm.context.Bootstrap;

public class App {
    public static void main(String[] args) throws Exception {
        @NotNull final Bootstrap bootstrap = new Bootstrap();
        bootstrap.init();
    }
}

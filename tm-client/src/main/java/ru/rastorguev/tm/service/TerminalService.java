package ru.rastorguev.tm.service;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.api.service.ITerminalService;

import java.util.Scanner;

@NoArgsConstructor
public final class TerminalService implements ITerminalService {

    @NotNull
    private final Scanner scanner = new Scanner(System.in);

    @NotNull
    public String nextLine() {
        return scanner.nextLine().trim();
    }

    public void close() {
        scanner.close();
    }
}
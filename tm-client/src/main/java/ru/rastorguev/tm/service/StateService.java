package ru.rastorguev.tm.service;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.api.service.IStateService;
import ru.rastorguev.tm.endpoint.Role;
import ru.rastorguev.tm.endpoint.Session;

import java.util.Arrays;
import java.util.List;

@NoArgsConstructor
public final class StateService implements IStateService {

    @Getter
    @Setter
    @Nullable
    private Session session;

    @Override
    public boolean isAuth() {
        return session != null;
    }

    @Override
    public boolean isRolesAllowed(@Nullable final Role... roles) {
        if (roles == null) return false;
        if (session == null || session.getRole() == null) return false;
        @Nullable final List<Role> listOfRoles = Arrays.asList(roles);
        return listOfRoles.contains(session.getRole());
    }


}

package ru.rastorguev.tm.error;

import lombok.NoArgsConstructor;

@NoArgsConstructor
public final class FailException extends Exception{

    public FailException(String message) {
        super(message);
    }
}

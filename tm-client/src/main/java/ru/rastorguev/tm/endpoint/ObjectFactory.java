
package ru.rastorguev.tm.endpoint;

import javax.xml.bind.JAXBElement;
import javax.xml.bind.annotation.XmlElementDecl;
import javax.xml.bind.annotation.XmlRegistry;
import javax.xml.namespace.QName;


/**
 * This object contains factory methods for each 
 * Java content interface and Java element interface 
 * generated in the ru.rastorguev.tm.endpoint package. 
 * <p>An ObjectFactory allows you to programatically 
 * construct new instances of the Java representation 
 * for XML content. The Java representation of XML 
 * content can consist of schema derived interfaces 
 * and classes representing the binding of schema 
 * type definitions, element declarations and model 
 * groups.  Factory methods for each of these are 
 * provided in this class.
 * 
 */
@XmlRegistry
public class ObjectFactory {

    private final static QName _Exception_QNAME = new QName("http://endpoint.tm.rastorguev.ru/", "Exception");
    private final static QName _CreateAdmin_QNAME = new QName("http://endpoint.tm.rastorguev.ru/", "createAdmin");
    private final static QName _CreateAdminResponse_QNAME = new QName("http://endpoint.tm.rastorguev.ru/", "createAdminResponse");
    private final static QName _CreateUser_QNAME = new QName("http://endpoint.tm.rastorguev.ru/", "createUser");
    private final static QName _CreateUserResponse_QNAME = new QName("http://endpoint.tm.rastorguev.ru/", "createUserResponse");
    private final static QName _FindCurrentUser_QNAME = new QName("http://endpoint.tm.rastorguev.ru/", "findCurrentUser");
    private final static QName _FindCurrentUserResponse_QNAME = new QName("http://endpoint.tm.rastorguev.ru/", "findCurrentUserResponse");
    private final static QName _IsExistByLogin_QNAME = new QName("http://endpoint.tm.rastorguev.ru/", "isExistByLogin");
    private final static QName _IsExistByLoginResponse_QNAME = new QName("http://endpoint.tm.rastorguev.ru/", "isExistByLoginResponse");
    private final static QName _RemoveAllUsers_QNAME = new QName("http://endpoint.tm.rastorguev.ru/", "removeAllUsers");
    private final static QName _RemoveAllUsersResponse_QNAME = new QName("http://endpoint.tm.rastorguev.ru/", "removeAllUsersResponse");
    private final static QName _UpdateUserLogin_QNAME = new QName("http://endpoint.tm.rastorguev.ru/", "updateUserLogin");
    private final static QName _UpdateUserLoginResponse_QNAME = new QName("http://endpoint.tm.rastorguev.ru/", "updateUserLoginResponse");
    private final static QName _UpdateUserPassword_QNAME = new QName("http://endpoint.tm.rastorguev.ru/", "updateUserPassword");
    private final static QName _UpdateUserPasswordResponse_QNAME = new QName("http://endpoint.tm.rastorguev.ru/", "updateUserPasswordResponse");

    /**
     * Create a new ObjectFactory that can be used to create new instances of schema derived classes for package: ru.rastorguev.tm.endpoint
     * 
     */
    public ObjectFactory() {
    }

    /**
     * Create an instance of {@link Exception }
     * 
     */
    public Exception createException() {
        return new Exception();
    }

    /**
     * Create an instance of {@link CreateAdmin }
     * 
     */
    public CreateAdmin createCreateAdmin() {
        return new CreateAdmin();
    }

    /**
     * Create an instance of {@link CreateAdminResponse }
     * 
     */
    public CreateAdminResponse createCreateAdminResponse() {
        return new CreateAdminResponse();
    }

    /**
     * Create an instance of {@link CreateUser }
     * 
     */
    public CreateUser createCreateUser() {
        return new CreateUser();
    }

    /**
     * Create an instance of {@link CreateUserResponse }
     * 
     */
    public CreateUserResponse createCreateUserResponse() {
        return new CreateUserResponse();
    }

    /**
     * Create an instance of {@link FindCurrentUser }
     * 
     */
    public FindCurrentUser createFindCurrentUser() {
        return new FindCurrentUser();
    }

    /**
     * Create an instance of {@link FindCurrentUserResponse }
     * 
     */
    public FindCurrentUserResponse createFindCurrentUserResponse() {
        return new FindCurrentUserResponse();
    }

    /**
     * Create an instance of {@link IsExistByLogin }
     * 
     */
    public IsExistByLogin createIsExistByLogin() {
        return new IsExistByLogin();
    }

    /**
     * Create an instance of {@link IsExistByLoginResponse }
     * 
     */
    public IsExistByLoginResponse createIsExistByLoginResponse() {
        return new IsExistByLoginResponse();
    }

    /**
     * Create an instance of {@link RemoveAllUsers }
     * 
     */
    public RemoveAllUsers createRemoveAllUsers() {
        return new RemoveAllUsers();
    }

    /**
     * Create an instance of {@link RemoveAllUsersResponse }
     * 
     */
    public RemoveAllUsersResponse createRemoveAllUsersResponse() {
        return new RemoveAllUsersResponse();
    }

    /**
     * Create an instance of {@link UpdateUserLogin }
     * 
     */
    public UpdateUserLogin createUpdateUserLogin() {
        return new UpdateUserLogin();
    }

    /**
     * Create an instance of {@link UpdateUserLoginResponse }
     * 
     */
    public UpdateUserLoginResponse createUpdateUserLoginResponse() {
        return new UpdateUserLoginResponse();
    }

    /**
     * Create an instance of {@link UpdateUserPassword }
     * 
     */
    public UpdateUserPassword createUpdateUserPassword() {
        return new UpdateUserPassword();
    }

    /**
     * Create an instance of {@link UpdateUserPasswordResponse }
     * 
     */
    public UpdateUserPasswordResponse createUpdateUserPasswordResponse() {
        return new UpdateUserPasswordResponse();
    }

    /**
     * Create an instance of {@link User }
     * 
     */
    public User createUser() {
        return new User();
    }

    /**
     * Create an instance of {@link Session }
     * 
     */
    public Session createSession() {
        return new Session();
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link Exception }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link Exception }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.rastorguev.ru/", name = "Exception")
    public JAXBElement<Exception> createException(Exception value) {
        return new JAXBElement<Exception>(_Exception_QNAME, Exception.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateAdmin }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link CreateAdmin }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.rastorguev.ru/", name = "createAdmin")
    public JAXBElement<CreateAdmin> createCreateAdmin(CreateAdmin value) {
        return new JAXBElement<CreateAdmin>(_CreateAdmin_QNAME, CreateAdmin.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateAdminResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link CreateAdminResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.rastorguev.ru/", name = "createAdminResponse")
    public JAXBElement<CreateAdminResponse> createCreateAdminResponse(CreateAdminResponse value) {
        return new JAXBElement<CreateAdminResponse>(_CreateAdminResponse_QNAME, CreateAdminResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateUser }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link CreateUser }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.rastorguev.ru/", name = "createUser")
    public JAXBElement<CreateUser> createCreateUser(CreateUser value) {
        return new JAXBElement<CreateUser>(_CreateUser_QNAME, CreateUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link CreateUserResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link CreateUserResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.rastorguev.ru/", name = "createUserResponse")
    public JAXBElement<CreateUserResponse> createCreateUserResponse(CreateUserResponse value) {
        return new JAXBElement<CreateUserResponse>(_CreateUserResponse_QNAME, CreateUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindCurrentUser }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link FindCurrentUser }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.rastorguev.ru/", name = "findCurrentUser")
    public JAXBElement<FindCurrentUser> createFindCurrentUser(FindCurrentUser value) {
        return new JAXBElement<FindCurrentUser>(_FindCurrentUser_QNAME, FindCurrentUser.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link FindCurrentUserResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link FindCurrentUserResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.rastorguev.ru/", name = "findCurrentUserResponse")
    public JAXBElement<FindCurrentUserResponse> createFindCurrentUserResponse(FindCurrentUserResponse value) {
        return new JAXBElement<FindCurrentUserResponse>(_FindCurrentUserResponse_QNAME, FindCurrentUserResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IsExistByLogin }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link IsExistByLogin }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.rastorguev.ru/", name = "isExistByLogin")
    public JAXBElement<IsExistByLogin> createIsExistByLogin(IsExistByLogin value) {
        return new JAXBElement<IsExistByLogin>(_IsExistByLogin_QNAME, IsExistByLogin.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link IsExistByLoginResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link IsExistByLoginResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.rastorguev.ru/", name = "isExistByLoginResponse")
    public JAXBElement<IsExistByLoginResponse> createIsExistByLoginResponse(IsExistByLoginResponse value) {
        return new JAXBElement<IsExistByLoginResponse>(_IsExistByLoginResponse_QNAME, IsExistByLoginResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveAllUsers }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RemoveAllUsers }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.rastorguev.ru/", name = "removeAllUsers")
    public JAXBElement<RemoveAllUsers> createRemoveAllUsers(RemoveAllUsers value) {
        return new JAXBElement<RemoveAllUsers>(_RemoveAllUsers_QNAME, RemoveAllUsers.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link RemoveAllUsersResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link RemoveAllUsersResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.rastorguev.ru/", name = "removeAllUsersResponse")
    public JAXBElement<RemoveAllUsersResponse> createRemoveAllUsersResponse(RemoveAllUsersResponse value) {
        return new JAXBElement<RemoveAllUsersResponse>(_RemoveAllUsersResponse_QNAME, RemoveAllUsersResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateUserLogin }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link UpdateUserLogin }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.rastorguev.ru/", name = "updateUserLogin")
    public JAXBElement<UpdateUserLogin> createUpdateUserLogin(UpdateUserLogin value) {
        return new JAXBElement<UpdateUserLogin>(_UpdateUserLogin_QNAME, UpdateUserLogin.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateUserLoginResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link UpdateUserLoginResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.rastorguev.ru/", name = "updateUserLoginResponse")
    public JAXBElement<UpdateUserLoginResponse> createUpdateUserLoginResponse(UpdateUserLoginResponse value) {
        return new JAXBElement<UpdateUserLoginResponse>(_UpdateUserLoginResponse_QNAME, UpdateUserLoginResponse.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateUserPassword }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link UpdateUserPassword }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.rastorguev.ru/", name = "updateUserPassword")
    public JAXBElement<UpdateUserPassword> createUpdateUserPassword(UpdateUserPassword value) {
        return new JAXBElement<UpdateUserPassword>(_UpdateUserPassword_QNAME, UpdateUserPassword.class, null, value);
    }

    /**
     * Create an instance of {@link JAXBElement }{@code <}{@link UpdateUserPasswordResponse }{@code >}
     * 
     * @param value
     *     Java instance representing xml element's value.
     * @return
     *     the new instance of {@link JAXBElement }{@code <}{@link UpdateUserPasswordResponse }{@code >}
     */
    @XmlElementDecl(namespace = "http://endpoint.tm.rastorguev.ru/", name = "updateUserPasswordResponse")
    public JAXBElement<UpdateUserPasswordResponse> createUpdateUserPasswordResponse(UpdateUserPasswordResponse value) {
        return new JAXBElement<UpdateUserPasswordResponse>(_UpdateUserPasswordResponse_QNAME, UpdateUserPasswordResponse.class, null, value);
    }

}

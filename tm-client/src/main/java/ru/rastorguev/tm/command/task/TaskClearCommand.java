package ru.rastorguev.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.command.AbstractCommand;
import ru.rastorguev.tm.endpoint.Project;
import ru.rastorguev.tm.endpoint.Role;
import ru.rastorguev.tm.endpoint.Session;
import ru.rastorguev.tm.error.FailException;

import java.util.List;

import static ru.rastorguev.tm.util.NumberUtil.isInt;
import static ru.rastorguev.tm.view.View.*;

@NoArgsConstructor
public final class TaskClearCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return true;
    }

    @NotNull
    @Override
    public String getName() {
        return "task_clear";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Remove project tasks.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("Task clear");

        @Nullable final Session session = serviceLocator.getStateService().getSession();
        if (session == null) throw new FailException("Log in to open new session.");

        System.out.println("Enter Project ID");
        @NotNull final List<Project> projectList =  serviceLocator.getProjectEndpoint().findAllProjectsForUser(session);
        printAllProjectsForUser(projectList);

        @NotNull final String input = serviceLocator.getTerminalService().nextLine();
        final int number = isInt(input);
        if (number == 0) throw new FailException("Entered value is not a number.");

        @Nullable final String projectId = serviceLocator.getProjectEndpoint().projectIdByNumber(session, number);
        if (projectId == null || projectId.isEmpty()) throw new FailException("Project could not be found.");

        serviceLocator.getTaskEndpoint().removeTaskListByProjectId(session, projectId);

        System.out.println("OK");
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMINISTRATOR, Role.USER };
    }
}
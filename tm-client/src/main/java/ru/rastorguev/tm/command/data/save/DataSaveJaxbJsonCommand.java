package ru.rastorguev.tm.command.data.save;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.command.AbstractCommand;
import ru.rastorguev.tm.endpoint.Role;
import ru.rastorguev.tm.endpoint.Session;
import ru.rastorguev.tm.error.FailException;

@NoArgsConstructor
public class DataSaveJaxbJsonCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public @NotNull String getName() {
        return "data_save_jaxb_json";
    }

    @Override
    public @NotNull String getDescription() {
        return "Save repositories JAX-B JSON";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("Data save JAX-B JSON");

        @Nullable final Session session = serviceLocator.getStateService().getSession();
        if (session == null) throw new FailException("Log in to open new session.");

        serviceLocator.getDataEndpoint().dataSaveJaxbJson(session);

        System.out.println("OK");
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMINISTRATOR };
    }
}

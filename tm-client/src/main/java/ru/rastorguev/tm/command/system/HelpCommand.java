package ru.rastorguev.tm.command.system;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.command.AbstractCommand;
import ru.rastorguev.tm.endpoint.Role;

import java.util.LinkedList;
import java.util.List;

@NoArgsConstructor
public final class HelpCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return false;
    }

    @NotNull
    @Override
    public String getName() {
        return "help";
    }

    @NotNull
    @Override
    public String getDescription() { return "Show all commands."; }

    @Override
    public void execute() throws Exception{
        @NotNull final List<AbstractCommand> AbstractCommandList =
                new LinkedList<>(serviceLocator.getCommandService().getCommandMap().values());
        for (final AbstractCommand command : AbstractCommandList) {
            System.out.println(command.getName() + ": " + command.getDescription());
        }
    }

    @Nullable
    @Override
    public Role[] roles() {
        return null;
    }
}
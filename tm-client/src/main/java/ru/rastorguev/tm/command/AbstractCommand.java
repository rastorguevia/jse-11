package ru.rastorguev.tm.command;

import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.api.service.ServiceLocator;
import ru.rastorguev.tm.endpoint.Role;

@NoArgsConstructor
public abstract class AbstractCommand {

    @Getter
    @Setter
    @NotNull
    protected ServiceLocator serviceLocator;

    public abstract boolean secure();
    public abstract @NotNull String getName();
    public abstract @NotNull String getDescription();
    public abstract void execute() throws Exception;
    public abstract @Nullable Role[] roles();
}
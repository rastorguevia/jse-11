package ru.rastorguev.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.command.AbstractCommand;
import ru.rastorguev.tm.endpoint.Project;
import ru.rastorguev.tm.endpoint.Role;
import ru.rastorguev.tm.endpoint.Session;
import ru.rastorguev.tm.endpoint.Task;
import ru.rastorguev.tm.error.FailException;

import java.util.List;

import static ru.rastorguev.tm.util.NumberUtil.isInt;
import static ru.rastorguev.tm.view.View.*;

@NoArgsConstructor
public final class TaskCreateCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return true;
    }

    @NotNull
    @Override
    public String getName() {
        return "task_create";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Create new task.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("Task create");

        @Nullable final Session session = serviceLocator.getStateService().getSession();
        if (session == null) throw new FailException("Log in to open new session.");
        @Nullable  final String userId = session.getUserId();
        if (userId == null || userId.isEmpty()) throw new FailException("Session is not valid.");

        System.out.println("Enter Project ID");
        @NotNull final List<Project> projectList =  serviceLocator.getProjectEndpoint().findAllProjectsForUser(session);
        printAllProjectsForUser(projectList);

        @NotNull final String input = serviceLocator.getTerminalService().nextLine();
        final int number = isInt(input);
        if (number == 0) throw new FailException("Entered value is not a number.");

        @Nullable final String projectId = serviceLocator.getProjectEndpoint().projectIdByNumber(session, number);
        if (projectId == null || projectId.isEmpty()) throw new FailException("Project could not be found.");
        @Nullable final Project project = serviceLocator.getProjectEndpoint().findProject(session, projectId);
        if (project == null) throw new FailException("Project does not exist.");

        System.out.println("Enter task name");
        @NotNull final String inputName = serviceLocator.getTerminalService().nextLine();
        if (inputName.isEmpty()) throw new FailException("Empty name. Try again.");

        System.out.println("Enter task description");
        @NotNull final String inputDescription = serviceLocator.getTerminalService().nextLine();
        if (inputDescription.isEmpty()) throw new FailException("Empty description. Try again.");

        @NotNull final Task task = new Task();
        task.setProjectId(projectId);
        task.setProjectName(project.getName());
        task.setUserId(userId);
        task.setName(inputName);
        task.setDescription(inputDescription);

        serviceLocator.getTaskEndpoint().createTask(session, task);

        System.out.println("OK");
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMINISTRATOR, Role.USER };
    }
}
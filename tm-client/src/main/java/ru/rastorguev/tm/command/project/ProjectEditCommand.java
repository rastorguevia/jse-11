package ru.rastorguev.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.command.AbstractCommand;
import ru.rastorguev.tm.endpoint.Project;
import ru.rastorguev.tm.endpoint.Role;
import ru.rastorguev.tm.endpoint.Session;
import ru.rastorguev.tm.endpoint.Status;
import ru.rastorguev.tm.enumerated.Confirmation;
import ru.rastorguev.tm.error.FailException;

import java.util.List;

import static ru.rastorguev.tm.util.DateUtil.*;
import static ru.rastorguev.tm.util.NumberUtil.isInt;
import static ru.rastorguev.tm.view.View.*;

@NoArgsConstructor
public final class ProjectEditCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return true;
    }

    @NotNull
    @Override
    public String getName() {
        return "project_edit";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Edit selected project.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("Project edit");
        System.out.println("Enter project ID");

        @Nullable final Session session = serviceLocator.getStateService().getSession();
        if (session == null) throw new FailException("Log in to open new session.");
        @Nullable  final String userId = session.getUserId();
        if (userId == null || userId.isEmpty()) throw new FailException("Session is not valid.");

        @NotNull final List<Project> listOfProjectsByUser = serviceLocator.getProjectEndpoint().findAllProjectsForUser(session);
        printAllProjectsForUser(listOfProjectsByUser);

        @NotNull final String input = serviceLocator.getTerminalService().nextLine();
        final int number = isInt(input);
        if (number == 0) throw new FailException("Entered value is not a number.");

        @Nullable final String projectId = serviceLocator.getProjectEndpoint().projectIdByNumber(session, number);
        if (projectId == null || projectId.isEmpty()) throw new FailException("Project could not be found.");
        @Nullable final Project project = serviceLocator.getProjectEndpoint().findProject(session, projectId);
        if (project == null) throw new FailException("Project does not exist.");

        @NotNull final Project editedProject = new Project();
        editedProject.setId(projectId);
        editedProject.setUserId(userId);

        try {
            System.out.println("Edit project name? Y/N");
            if (Confirmation.Y.equals(Confirmation.valueOf(serviceLocator.getTerminalService().nextLine().toUpperCase()))) {
                System.out.println("Enter project name");
                @NotNull final String projectNewName = serviceLocator.getTerminalService().nextLine();
                editedProject.setName(projectNewName);
            } else editedProject.setName(project.getName());
        } catch (IllegalArgumentException e) {
            System.out.println("You entered an incorrect statement");
            editedProject.setName(project.getName());
        }

        try {
            System.out.println("Edit description? Y/N");
            if (Confirmation.Y.equals(Confirmation.valueOf(serviceLocator.getTerminalService().nextLine().toUpperCase()))) {
                System.out.println("Enter new description");
                @NotNull final String projectNewDescription = serviceLocator.getTerminalService().nextLine();
                editedProject.setDescription(projectNewDescription);
            } else editedProject.setDescription(project.getDescription());
        } catch (IllegalArgumentException e) {
            System.out.println("You entered an incorrect statement");
            editedProject.setDescription(project.getDescription());
        }

        System.out.println("Enter start date  Format: dd.mm.yyyy");
        @NotNull final String projectNewStartDate = serviceLocator.getTerminalService().nextLine();
        editedProject.setStartDate(stringToDate(projectNewStartDate));

        System.out.println("Enter end date  Format: dd.mm.yyyy");
        @NotNull final String projectNewEndDate = serviceLocator.getTerminalService().nextLine();
        editedProject.setEndDate(stringToDate(projectNewEndDate));

        System.out.println("Enter status: Planned, In-Progress, Done.");
        @NotNull final String status = serviceLocator.getTerminalService().nextLine();
        if ("planned".equals(status.toLowerCase())) editedProject.setStatus(Status.PLANNED);
        if ("in-progress".equals(status.toLowerCase())) editedProject.setStatus(Status.IN_PROGRESS);
        if ("done".equals(status.toLowerCase())) editedProject.setStatus(Status.DONE);
        else throw new FailException("Wrong status.");

        serviceLocator.getProjectEndpoint().updateProject(session, editedProject);

        System.out.println("OK");
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMINISTRATOR, Role.USER };
    }
}
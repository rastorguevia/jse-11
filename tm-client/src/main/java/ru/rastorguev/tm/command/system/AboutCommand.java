package ru.rastorguev.tm.command.system;

import com.jcabi.manifests.Manifests;
import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.command.AbstractCommand;
import ru.rastorguev.tm.endpoint.Role;

@NoArgsConstructor
public final class AboutCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return false;
    }

    @NotNull
    @Override
    public String getName() {
        return "about";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Build number info.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("Info");
        @NotNull final String projectVersion = Manifests.read("projectVersion");
        @NotNull final String buildNumber = Manifests.read("buildNumber");
        @NotNull final String developer = Manifests.read("developer");
        System.out.println("projectVersion: " + projectVersion + "\n" +"build number: " + buildNumber
                + "\n" + "developer: " + developer);
    }

    @Nullable
    @Override
    public Role[] roles() {
        return null;
    }
}

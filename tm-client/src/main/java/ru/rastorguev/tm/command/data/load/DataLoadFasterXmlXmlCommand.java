package ru.rastorguev.tm.command.data.load;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.command.AbstractCommand;

import ru.rastorguev.tm.endpoint.Role;
import ru.rastorguev.tm.endpoint.Session;
import ru.rastorguev.tm.error.FailException;

@NoArgsConstructor
public class DataLoadFasterXmlXmlCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public @NotNull String getName() {
        return "data_load_fasterxml_xml";
    }

    @Override
    public @NotNull String getDescription() {
        return "Load repositories FasterXML XML";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("Data load FasterXML XML");

        @Nullable final Session session = serviceLocator.getStateService().getSession();
        if (session == null) throw new FailException("Log in to open new session.");

        serviceLocator.getDataEndpoint().dataLoadFasterXmlXml(session);

        System.out.println("OK");
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMINISTRATOR };
    }
}

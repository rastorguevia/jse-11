package ru.rastorguev.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.command.AbstractCommand;
import ru.rastorguev.tm.endpoint.Role;
import ru.rastorguev.tm.endpoint.Session;
import ru.rastorguev.tm.error.FailException;

@NoArgsConstructor
public final class UserPasswordUpdateCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return true;
    }

    @NotNull
    @Override
    public String getName() {
        return "password_update";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Update password.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("Password update");

        @Nullable final Session session = serviceLocator.getStateService().getSession();
        if (session == null) throw new FailException("Log in to open new session.");

        System.out.println("Enter new password");
        @NotNull final String password = serviceLocator.getTerminalService().nextLine();
        if (password.isEmpty()) throw new FailException("Empty password. Try again");

        System.out.println("Enter old password");
        @NotNull final String oldPassword = serviceLocator.getTerminalService().nextLine();
        if (oldPassword.isEmpty()) throw new FailException("Empty old password. Try again");

        serviceLocator.getUserEndpoint().updateUserPassword(session, password, oldPassword);

        System.out.println("OK");
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMINISTRATOR, Role.USER };
    }
}

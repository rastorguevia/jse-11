package ru.rastorguev.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.command.AbstractCommand;
import ru.rastorguev.tm.endpoint.Project;
import ru.rastorguev.tm.endpoint.Role;
import ru.rastorguev.tm.endpoint.Session;
import ru.rastorguev.tm.endpoint.Task;
import ru.rastorguev.tm.error.FailException;

import java.util.List;

import static ru.rastorguev.tm.util.NumberUtil.isInt;
import static ru.rastorguev.tm.view.View.*;

@NoArgsConstructor
public final class TaskSelectCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return true;
    }

    @NotNull
    @Override
    public String getName() {
        return "task_select";
    }

    @NotNull
    @Override
    public String getDescription() { return "Select exact task."; }

    @Override
    public void execute() throws Exception {
        System.out.println("Task select");

        @Nullable final Session session = serviceLocator.getStateService().getSession();
        if (session == null) throw new FailException("Log in to open new session.");

        System.out.println("Enter Project ID");
        @NotNull final List<Project> projectList =  serviceLocator.getProjectEndpoint().findAllProjectsForUser(session);
        printAllProjectsForUser(projectList);

        @NotNull final String inputProjectNumber = serviceLocator.getTerminalService().nextLine();
        final int projectNumber = isInt(inputProjectNumber);
        if (projectNumber == 0) throw new FailException("Entered value is not a number.");

        @Nullable final String projectId = serviceLocator.getProjectEndpoint().projectIdByNumber(session, projectNumber);
        if (projectId == null || projectId.isEmpty()) throw new FailException("Project could not be found.");

        @NotNull final List<Task> taskListByProjectId = serviceLocator.getTaskEndpoint().getTaskListByProjectId(session, projectId);
        printTaskListByProjectId(taskListByProjectId);

        System.out.println("Enter Task ID");
        @NotNull final String inputTaskNumber = serviceLocator.getTerminalService().nextLine();
        final int taskNumber = isInt(inputTaskNumber);
        if (taskNumber == 0) throw new FailException("Entered value is not a number.");

        @Nullable final String taskId = serviceLocator.getTaskEndpoint().taskIdByNumberAndProjectId(session, taskNumber, projectId);
        if (taskId == null || taskId.isEmpty()) throw new FailException("Task could not be found.");
        @Nullable final Task task = serviceLocator.getTaskEndpoint().findTask(session, taskId);
        if (task == null) throw new FailException("Task does not exist.");
        printTask(task);

        System.out.println("OK");
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMINISTRATOR, Role.USER };
    }
}
package ru.rastorguev.tm.command.data;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.command.AbstractCommand;
import ru.rastorguev.tm.endpoint.Role;
import ru.rastorguev.tm.endpoint.Session;
import ru.rastorguev.tm.error.FailException;

public class DataClearAllCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return true;
    }

    @Override
    public @NotNull String getName() {
        return "clear_all";
    }

    @Override
    public @NotNull String getDescription() {
        return "Clear all repositories.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("Clear All");

        @Nullable final Session session = serviceLocator.getStateService().getSession();
        if (session == null) throw new FailException("Log in to open new session.");

        serviceLocator.getUserEndpoint().removeAllUsers(session);
        serviceLocator.getProjectEndpoint().removeAllProjects(session);
        serviceLocator.getTaskEndpoint().removeAllTasks(session);

        System.out.println("OK");
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMINISTRATOR };
    }
}

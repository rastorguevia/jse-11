package ru.rastorguev.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.command.AbstractCommand;
import ru.rastorguev.tm.endpoint.*;
import ru.rastorguev.tm.enumerated.Confirmation;
import ru.rastorguev.tm.error.FailException;

import java.lang.Exception;
import java.util.List;

import static ru.rastorguev.tm.util.DateUtil.*;
import static ru.rastorguev.tm.util.NumberUtil.isInt;
import static ru.rastorguev.tm.view.View.*;

@NoArgsConstructor
public final class TaskEditCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return true;
    }

    @NotNull
    @Override
    public String getName() {
        return "task_edit";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Edit selected task.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("Task edit");

        @Nullable final Session session = serviceLocator.getStateService().getSession();
        if (session == null) throw new FailException("Log in to open new session.");
        @Nullable  final String userId = session.getUserId();
        if (userId == null || userId.isEmpty()) throw new FailException("Session is not valid.");

        System.out.println("Enter Project ID");
        @NotNull final List<Project> projectList =  serviceLocator.getProjectEndpoint().findAllProjectsForUser(session);
        printAllProjectsForUser(projectList);

        @NotNull final String inputProjectNumber = serviceLocator.getTerminalService().nextLine();
        final int projectNumber = isInt(inputProjectNumber);
        if (projectNumber == 0) throw new FailException("Entered value is not a number.");

        @Nullable final String projectId = serviceLocator.getProjectEndpoint().projectIdByNumber(session, projectNumber);
        if (projectId == null || projectId.isEmpty()) throw new FailException("Project could not be found.");

        @NotNull final List<Task> taskListByProjectId = serviceLocator.getTaskEndpoint().getTaskListByProjectId(session, projectId);
        printTaskListByProjectId(taskListByProjectId);

        System.out.println("Enter Task ID");
        @NotNull final String inputTaskNumber = serviceLocator.getTerminalService().nextLine();
        final int taskNumber = isInt(inputTaskNumber);
        if (taskNumber == 0) throw new FailException("Entered value is not a number.");

        @Nullable final String taskId = serviceLocator.getTaskEndpoint().taskIdByNumberAndProjectId(session, taskNumber, projectId);
        if (taskId == null || taskId.isEmpty()) throw new FailException("Task could not be found.");
        @Nullable final Task task = serviceLocator.getTaskEndpoint().findTask(session, taskId);
        if (task == null) throw new FailException("Task does not exist.");

        @NotNull final Task editedTask = new Task();
        editedTask.setProjectId(projectId);
        editedTask.setProjectName(task.getProjectName());
        editedTask.setId(task.getId());
        editedTask.setUserId(userId);

        try {
            System.out.println("Edit task name? Y/N");
            if (Confirmation.Y.equals(Confirmation.valueOf(serviceLocator.getTerminalService().nextLine().toUpperCase()))) {
                System.out.println("Enter task name");
                @NotNull final String taskNewName = serviceLocator.getTerminalService().nextLine();
                editedTask.setName(taskNewName);
            } else editedTask.setName(task.getName());
        } catch (IllegalArgumentException e) {
            System.out.println("You entered an incorrect statement");
            editedTask.setName(task.getName());
        }

        try {
            System.out.println("Edit description? Y/N");
            if (Confirmation.Y.equals(Confirmation.valueOf(serviceLocator.getTerminalService().nextLine().toUpperCase()))) {
                System.out.println("Enter new description");
                @NotNull final String taskNewDescription = serviceLocator.getTerminalService().nextLine();
                editedTask.setDescription(taskNewDescription);
            } else editedTask.setDescription(task.getDescription());
        } catch (IllegalArgumentException e) {
            System.out.println("You entered an incorrect statement");
            editedTask.setDescription(task.getDescription());
        }

        System.out.println("Enter start date  Format: dd.mm.yyyy");
        @NotNull final String taskNewStartDate = serviceLocator.getTerminalService().nextLine();
        editedTask.setStartDate(stringToDate(taskNewStartDate));

        System.out.println("Enter end date  Format: dd.mm.yyyy");
        @NotNull final String taskNewEndDate = serviceLocator.getTerminalService().nextLine();
        editedTask.setEndDate(stringToDate(taskNewEndDate));

        System.out.println("Enter status: Planned, In-Progress, Done.");
        @NotNull final String status = serviceLocator.getTerminalService().nextLine();
        if ("planned".equals(status.toLowerCase())) editedTask.setStatus(Status.PLANNED);
        if ("in-progress".equals(status.toLowerCase())) editedTask.setStatus(Status.IN_PROGRESS);
        if ("done".equals(status.toLowerCase())) editedTask.setStatus(Status.DONE);
        else throw new FailException("Wrong status.");

        serviceLocator.getTaskEndpoint().updateTask(session, editedTask);

        System.out.println("OK");
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMINISTRATOR, Role.USER };
    }
}
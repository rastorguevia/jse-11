package ru.rastorguev.tm.command.project;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.command.AbstractCommand;
import ru.rastorguev.tm.endpoint.Project;
import ru.rastorguev.tm.endpoint.Role;
import ru.rastorguev.tm.endpoint.Session;
import ru.rastorguev.tm.error.FailException;

import java.util.List;

import static ru.rastorguev.tm.view.View.*;

@NoArgsConstructor
public final class ProjectListCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return true;
    }

    @NotNull
    @Override
    public String getName() {
        return "project_list";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Show all projects.";
    }

    @Override
    public void execute() throws Exception{
        System.out.println("Project list");

        @Nullable final Session session = serviceLocator.getStateService().getSession();
        if (session == null) throw new FailException("Log in to open new session.");

        @NotNull final List<Project> projectList =  serviceLocator.getProjectEndpoint().findAllProjectsForUser(session);
        printAllProjectsForUser(projectList);

        System.out.println("OK");
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMINISTRATOR, Role.USER };
    }
}
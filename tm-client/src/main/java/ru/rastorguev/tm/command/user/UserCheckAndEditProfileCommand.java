package ru.rastorguev.tm.command.user;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.command.AbstractCommand;
import ru.rastorguev.tm.endpoint.Role;
import ru.rastorguev.tm.endpoint.Session;
import ru.rastorguev.tm.endpoint.User;
import ru.rastorguev.tm.error.FailException;

import static ru.rastorguev.tm.view.View.*;

@NoArgsConstructor
public final class UserCheckAndEditProfileCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return true;
    }

    @NotNull
    @Override
    public String getName() {
        return "check_edit";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Check and edit current profile.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("Check and edit");

        @Nullable final Session session = serviceLocator.getStateService().getSession();
        if (session == null) throw new FailException("Log in to open new session.");
        @Nullable final User user = serviceLocator.getUserEndpoint().findCurrentUser(session);
        if (user == null) throw new FailException("Something go wrong. Try to log in again.");
        printUserProfile(user);

        System.out.println("Enter new login");
        @NotNull final String login = serviceLocator.getTerminalService().nextLine();

        final boolean isLoginExist = serviceLocator.getUserEndpoint().isExistByLogin(session, login);
        if (isLoginExist) {
            System.out.println("This login already exist.");
            return;
        } else user.setLogin(login);

        serviceLocator.getUserEndpoint().updateUserLogin(session, login);

        System.out.println("OK");
    }

    @Nullable
    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMINISTRATOR, Role.USER };
    }
}
package ru.rastorguev.tm.command.task;

import lombok.NoArgsConstructor;
import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;
import ru.rastorguev.tm.command.AbstractCommand;
import ru.rastorguev.tm.endpoint.Role;
import ru.rastorguev.tm.endpoint.Session;
import ru.rastorguev.tm.endpoint.Task;
import ru.rastorguev.tm.error.FailException;

import java.util.List;

import static ru.rastorguev.tm.view.View.printTasksByWord;

@NoArgsConstructor
public final class TaskFindCommand extends AbstractCommand {

    @Override
    public boolean secure() {
        return true;
    }

    @NotNull
    @Override
    public String getName() {
        return "task_find";
    }

    @NotNull
    @Override
    public String getDescription() {
        return "Find task by part of title or description.";
    }

    @Override
    public void execute() throws Exception {
        System.out.println("Find task");

        @Nullable final Session session = serviceLocator.getStateService().getSession();
        if (session == null) throw new FailException("Log in to open new session.");

        System.out.println("Enter part of title or description");
        @NotNull final String input = serviceLocator.getTerminalService().nextLine();
        @NotNull final List<Task> filteredListOfTasks = serviceLocator.getTaskEndpoint().filteredTaskListByInput(session, input);
        printTasksByWord(filteredListOfTasks);

        System.out.println("OK");
    }

    @Override
    public Role[] roles() {
        return new Role[] { Role.ADMINISTRATOR, Role.USER };
    }
}
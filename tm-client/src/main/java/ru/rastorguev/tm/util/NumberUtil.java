package ru.rastorguev.tm.util;

import org.jetbrains.annotations.NotNull;

public final class NumberUtil {

    public static int isInt(@NotNull final String number) {
        int digit = 0;
        try {
            digit = Integer.parseInt(number);
        } catch (NumberFormatException e) {
            System.out.println("Entered a wrong number.");
        }
        return digit;
    }

}

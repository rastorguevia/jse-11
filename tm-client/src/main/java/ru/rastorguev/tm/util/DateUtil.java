package ru.rastorguev.tm.util;

import org.jetbrains.annotations.NotNull;
import org.jetbrains.annotations.Nullable;

import javax.xml.datatype.DatatypeConfigurationException;
import javax.xml.datatype.DatatypeFactory;
import javax.xml.datatype.XMLGregorianCalendar;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.GregorianCalendar;

public final class DateUtil {

    @NotNull
    public final static SimpleDateFormat dateFormatter = new SimpleDateFormat("d.MM.y");

    @Nullable
    public static XMLGregorianCalendar stringToDate (@NotNull final String date) {
        try {
            @NotNull final Date dateSimple = dateFormatter.parse(date);
            @NotNull final GregorianCalendar calendar = new GregorianCalendar();
            calendar.setTimeInMillis(dateSimple.getTime());
            return DatatypeFactory.newInstance().newXMLGregorianCalendar(calendar);
        } catch (ParseException | DatatypeConfigurationException e) {
            return null;
        }
    }
}